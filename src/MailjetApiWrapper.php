<?php

namespace Drupal\mailjet_webform_subscription;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Mailjet\Client;
use Mailjet\Resources;

/**
 * Service to connect to mailjet api.
 */
class MailjetApiWrapper implements IMailjetApiWrapperInterface {

  use StringTranslationTrait;

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $mailjetApiConfig;
  /**
   * Mailjet client.
   *
   * @var \Mailjet\Client
   */
  protected $mailJetApiClient;

  /**
   * The mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Drupal logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  public function __construct($mailjetApiConfig, $mailManager, $logger, $user) {
    $this->mailjetApiConfig = $mailjetApiConfig;
    $this->mailManager = $mailManager;
    $this->logger = $logger;
    $this->user = $user;

    $this->mailJetApiClient = new Client(
      $this->mailjetApiConfig->get('api_key_public'),
      $this->mailjetApiConfig->get('api_key_secret'),
      TRUE,
      ['version' => 'v3']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function appendToList(string $email_address, string|null $first_name, string|null $last_name, int $list_id) : bool {
    $fullName = implode(' ', array_filter([$first_name, $last_name]));

    $body = [
      'Action' => 'addnoforce',
      'Email' => $email_address,
    ];

    if ($first_name) {
      $body['Properties']['firstname'] = $first_name;
    }
    if ($last_name) {
      $body['Properties']['lastname'] = $last_name;
    }
    if ($fullName) {
      $body['Name'] = $fullName;
    }

    $response = $this->mailJetApiClient->post(Resources::$ContactslistManagecontact, ['id' => $list_id, 'body' => $body]);

    if (!$response->success()) {
      $this->logger->error($this->t(
        'Error appending @user_email to list @list_id. @message',
        ['@user_email' => $email_address, '@list_id' => $list_id, '@message' => $response]
      ));
    }

    return $response->success();
  }

  /**
   * {@inheritdoc}
   */
  public function sendTemplateEmail(string $recipient_email, int $template_id, array $variables = []): void {
    $template_details = $this->getMailJetTemplateDetails($template_id);

    if (!$template_details) {
      return;
    }

    if (!$template_details['Subject'] || !$template_details['From']) {
      $this->logger->error(
        $this->t(
          'Mailjet Template missing "Subject" and/or "From" param/s. Template ID: @templateId, Email:@email',
          [
            '@templateId' => $template_id,
            '@email' => $recipient_email,
          ]
        )
      );
      return;
    }

    $this->mailManager->mail(
      'mailjet_webform_subscription',
      'send_using_mailjet_template',
      $recipient_email,
      $this->user->getPreferredLangcode(),
      [
        'subject' => $template_details['Subject'],
        'from' => $template_details['SenderEmail'],
        'params' => [
          'TemplateId' => $template_id,
          'Variables' => $variables,
        ],
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMailJetLists() : array {
    $response = $this->mailJetApiClient->get(Resources::$Contactslist, ['filters' => ['limit' => 1000]]);

    if (!$response->success()) {
      throw new \Exception('Failed to fetch mailjet lists');
    }

    $result = [];
    foreach ($response->getData() as $list) {
      $result[$list['ID']] = $list['Name'] . ' (' . $list['ID'] . ')';
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getMailjetTemplates() : array {
    $response = $this->mailJetApiClient->get(Resources::$Template, ['filters' => ['limit' => 1000]]);

    if (!$response->success()) {
      throw new \Exception('Failed to fetch mailjet templates');
    }

    $result = [];
    foreach ($response->getData() as $template) {
      $result[$template['ID']] = $template['Name'] . ' (' . $template['ID'] . ')';
    }

    return $result;
  }

  /**
   * Get extra information about the template.
   */
  private function getMailJetTemplateDetails(string $template_id) : array | bool {
    $response = $this->mailJetApiClient->get(Resources::$TemplateDetailcontent, ['id' => $template_id]);

    if (!$response->success()) {
      $this->logger->error(
        $this->t(
          'Failed to fetch template details. Template ID: @templateId',
          [
            '@templateId' => $template_id,
          ]
        )
      );
      return FALSE;
    }

    $data = $response->getData();

    if (count($data) < 1) {
      $this->logger->error(
        $this->t(
          'Specified template not found. Template ID: @templateId',
          [
            '@templateId' => $template_id,
          ]
        )
      );
      return FALSE;
    }

    return $data[0]['Headers'];
  }

}
