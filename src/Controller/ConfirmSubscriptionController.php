<?php

namespace Drupal\mailjet_webform_subscription\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for spawning a new Qaack instance using link from email.
 */
class ConfirmSubscriptionController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Cache kill switch service.
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected $killSwitch;

  /**
   * The webform theme manager.
   *
   * @var \Drupal\webform\WebformThemeManagerInterface
   */
  protected $themeManager;


  /**
   * Drupal logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * The mailjet api wrapper.
   *
   * @var \Drupal\mailjet_webform_subscription\IMailjetApiWrapperInterface
   */
  protected $mailjetApiWrapper;

  public function __construct($database, $killSwitch, $themeManager, $user, $logger, $mailjetApiWrapper) {
    $this->database = $database;
    $this->killSwitch = $killSwitch;
    $this->themeManager = $themeManager;
    $this->user = $user;
    $this->logger = $logger;
    $this->mailjetApiWrapper = $mailjetApiWrapper;
  }

  /**
   * Dependency injector.
   */
  public static function create(ContainerInterface $container) : ConfirmSubscriptionController {
    return new static(
      $container->get('database'),
      $container->get('page_cache_kill_switch'),
      $container->get('webform.theme_manager'),
      $container->get('current_user'),
      $container->get('logger.factory')->get('mailjet_webform_subscription'),
      $container->get('mailjet_webform_subscription.mailjet_api_wrapper')
    );
  }

  /**
   * Route render method.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   Returns displayed content.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function content(Request $request) : array {
    $this->killSwitch->trigger();
    $token = $request->query->get('token', '');
    $token = preg_replace("/[^A-Za-z0-9 ]/", '', $token);
    $caller = $request->query->get('caller', '');
    $caller = preg_replace("/[^_a-z0-9 ]/", '', $caller);

    if (empty($token)) {
      return $this->generateResponse('No token provided.');
    }

    if (empty($caller)) {
      return $this->generateResponse('Caller not provided.');
    }

    $submissions = $this->database->select('webform_submission_data', 'wsd')
      ->condition('wsd.value', $token)
      ->condition('wsd.name', $caller)
      ->range(0, 1)
      ->fields('wsd', ['sid'])
      ->execute()
      ->fetchAllKeyed(0, 0);

    if (empty($submissions)) {
      return $this->generateResponse('The provided token is invalid.');
    }

    $submission = \reset($submissions);
    $submission = $this->entityTypeManager()->getStorage('webform_submission')->load($submission);
    $data = $submission->getRawData();

    if (!$submission instanceof WebformSubmissionInterface) {
      return $this->generateResponse('An issue occurred when subscribing to mail list.');
    }

    $checkbox_config = $submission->getWebform()->getElement($caller);

    if (!$checkbox_config) {
      return $this->generateResponse('The provided caller is invalid.');
    }

    if ($data[$caller]['token_consumed'] ?? FALSE) {
      return $this->generateResponse('The provided token has already been used. Please request a new token by re-subscribing.');
    }

    $data[$caller]['token_consumed'] = time();
    $submission->setData($data);
    $submission->save();
    $success = $this->mailjetApiWrapper->appendToList(
      $data[$checkbox_config['#email_address_field']],
      $data[$checkbox_config['#first_name_field'] ?? NULL] ?? NULL,
      $data[$checkbox_config['#last_name_field'] ?? NULL] ?? NULL,
      intval($checkbox_config['#list_id'])
    );

    if (!$success) {
      return $this->generateResponse('An issue occurred when subscribing to mail list.');
    }

    $variables = [];
    if ($checkbox_config['#template_name_variable']) {
      $variables[$checkbox_config['#template_name_variable']] = $data[$checkbox_config['#first_name_field'] ?? NULL] ?? '';
    }

    if ($checkbox_config['#use_success'] && ($checkbox_config['#success_template_id'] ?? FALSE)) {
      $this->mailjetApiWrapper->sendTemplateEmail(
        $data[$checkbox_config['#email_address_field']],
        intval($checkbox_config['#success_template_id']),
        $variables,
      );
    }

    if ($checkbox_config['#success_node_id'] ?? FALSE) {
      $node = $this->entityTypeManager()->getStorage('node')->load(intval($checkbox_config['#success_node_id']));
      if (!$node || !$node->isPublished() || !$node->access('view')) {
        throw new NotFoundHttpException();
      }
      $build = $this->entityTypeManager()->getViewBuilder('node')->view($node);
      $build['#cache']['max-age'] = 0;
      return $build;
    }

    return $this->generateResponse('Thank you for subscribing to our newsletter!');
  }

  /**
   * Process the response to template or node.
   *
   * @param string $message
   *   Message to send to theme.
   */
  protected function generateResponse(string $message = '') : array {
    $message = $message ?? '';
    return [
      '#theme' => 'mailjet_newsletter_subscription_response',
      '#attached' => [
        'library' => [
          'mailjet_webform_subscription/mailjet_response',
        ],
      ],
      // @codingStandardsIgnoreStart
      '#message' => $this->t($message),
      // @codingStandardsIgnoreEnd
      // This is crude but render array will not vary otherwise.
      '#cache' => ['max-age' => 0],
    ];
  }

}
