<?php

namespace Drupal\mailjet_webform_subscription\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\webform\Plugin\WebformElement\WebformCompositeBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an 'mail_list_checkbox' element.
 *
 * @WebformElement(
 *   id = "mail_list_checkbox",
 *   label = @Translation("Mailjet Subscription"),
 *   description = @Translation("Provides a checkbox that lets the user decide whether to subscribe to the mailjet newsletter."),
 *   category = @Translation("Mailjet Webform Subscription"),
 *   multiline = TRUE,
 *   composite = TRUE,
 *   states_wrapper = TRUE,
 * )
 */
class WebformMailListCheckbox extends WebformCompositeBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $user;


  /**
   * The mailjet api wrapper.
   *
   * @var \Drupal\mailjet_webform_subscription\IMailjetApiWrapperInterface
   */
  protected $mailjetApiWrapper;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) : WebformMailListCheckbox {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->user = $container->get('current_user');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->mailjetApiWrapper = $container->get('mailjet_webform_subscription.mailjet_api_wrapper');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginLabel() {
    return $this->moduleHandler->moduleExists('mail_list_checkbox') ? $this->t('Newsletter checkbox') : parent::getPluginLabel();
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface|null $webform_submission = NULL) : void {
    parent::prepare($element, $webform_submission);

    $element['#theme_wrappers'] = [];
    $element['#attached']['library'][] = 'mailjet_webform_subscription/mailjet_checkbox_composite';

    // #title display defaults to invisible.
    $element += [
      '#title_display' => 'invisible',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() : array {
    $properties = [
        // Element settings.
      'title' => '',
      'user_confirmation__title' => '',
      'user_confirmation__help' => '',
      'user_confirmation__required' => FALSE,
      'user_confirmation__required_error' => NULL,
      'first_name_field' => '',
      'last_name_field' => '',
      'email_address_field' => '',
      'use_confirmation' => FALSE,
      'use_success' => FALSE,
      'warning' => '',
      'confirm_template_id' => '',
      'success_template_id' => '',
      'template_name_variable' => '',
      'template_redirect_variable' => '',
      'always_subscribe' => FALSE,
      'list_id' => '',
      'success_node_id' => '',
    ];
    unset($properties['multiple__header']);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) : array {

    $machine_name_field_options = $this->getAvailableFormFields();
    $template_options = $this->mailjetApiWrapper->getMailjetTemplates();
    $list_options = $this->mailjetApiWrapper->getMailJetLists();

    $form = parent::form($form, $form_state);

    unset($form['composite']);
    unset($form['validation']);

    // Rendered checkbox.
    $form['confirmation_specific'] = [
      '#type' => 'details',
      '#title' => $this->t('Rendered checkbox'),
      '#help' => $this->t('The checkbox that the user sees.'),
      '#open' => TRUE,
    ];
    $form['confirmation_specific']['always_subscribe'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always subscribe'),
      '#description' => $this->t('Hide checkbox and always subscribe contact on form submission'),
      '#required' => FALSE,
    ];
    $form['confirmation_specific']['user_confirmation__required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Required'),
      '#help' => $this->t('Marks the checkbox as a required field.'),
      '#states' => [
        'visible' => [
          [':input[name="properties[always_subscribe]"]' => ['checked' => FALSE]],
        ],
      ],
    ];
    $form['confirmation_specific']['user_confirmation__required_error'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Required message'),
      '#description' => $this->t('If set, this message will be used when a required webform element is empty, instead of the default "Field x is required." message.'),
      '#states' => [
        'visible' => [
          [':input[name="properties[user_confirmation__required]"]' => ['checked' => TRUE]],
        ],
      ],
    ];
    $form['confirmation_specific']['user_confirmation__title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Checkbox label'),
      '#description' => $this->t('The checkbox label displayed to user'),
      '#maxlength' => 255,
      '#states' => [
        'visible' => [
          [':input[name="properties[always_subscribe]"]' => ['checked' => FALSE]],
        ],
      ],
    ];
    $form['confirmation_specific']['user_confirmation__help'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Help text for checkbox'),
      '#description' => $this->t('Field in this form that contains the users last name'),
      '#maxlength' => 255,
      '#states' => [
        'visible' => [
          [':input[name="properties[always_subscribe]"]' => ['checked' => FALSE]],
        ],
      ],
    ];

    // Machine names.
    $form['machine_names'] = [
      '#type' => 'details',
      '#title' => $this->t('Form field mapping'),
      '#help' => $this->t('Used to map to the machine names that are used within the form'),
      '#open' => TRUE,
    ];
    $form['machine_names']['first_name_field'] = [
      '#type' => 'select',
      '#title' => $this->t('First name field'),
      '#description' => $this->t('Field in this form that contains the users first name'),
      '#empty_option' => $this->t('- None -'),
      '#options' => $machine_name_field_options,
    ];
    $form['machine_names']['last_name_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Last name field'),
      '#description' => $this->t('Field in this form that contains the users last name'),
      '#empty_option' => $this->t('- None -'),
      '#options' => $machine_name_field_options,
    ];
    $form['machine_names']['email_address_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Email address field'),
      '#description' => $this->t('Field in this form that contains the users email address'),
      '#options' => $machine_name_field_options,
      '#required' => TRUE,
    ];

    // Template configuration.
    $form['template_configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Send client emails'),
      '#help' => $this->t('Configuration and setup of the emails used and their templates.'),
      '#open' => TRUE,
    ];
    $form['template_configuration']['use_confirmation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send confirmation email'),
      '#description' => $this->t('Send a confirmation email to the user before subscribing them to a list.'),
      '#required' => FALSE,
    ];
    $form['template_configuration']['use_success'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send success email'),
      '#description' => $this->t('Send a email to the user informing them that they have successfully subscribed to the list.'),
      '#required' => FALSE,
    ];
    $form['template_configuration']['confirmation_warning'] = [
      '#type' => 'webform_message',
      '#message_type' => 'warning',
      '#message_message' => $this->t('Depending on the use of the mail list, an email confirming the subscription may be a legal requirement in your area.'),
      '#access' => TRUE,
      '#states' => [
        'visible' => [
          [':input[name="properties[use_confirmation]"]' => ['checked' => FALSE]],
        ],
      ],
    ];
    $form['template_configuration']['template_name_variable'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Template name variable'),
      '#description' => $this->t('The variable to hold the name in the confirmation and success template eg {{var:first_name}}'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          [':input[name="properties[use_confirmation]"]' => ['checked' => TRUE]],
          'or',
          [':input[name="properties[use_success]"]' => ['checked' => TRUE]],
        ],
      ],
    ];
    $form['template_configuration']['template_redirect_variable'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Template redirect variable'),
      '#description' => $this->t('The variable to hold the redirect url used in the confirmation template eg {{var:redirect_url}}'),
      '#states' => [
        'visible' => [
          ':input[name="properties[use_confirmation]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="properties[use_confirmation]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['template_configuration']['confirm_template_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Mailjet template for confirmation email'),
      '#description' => $this->t('The ID of the mailjet template to use for email confirmation'),
      '#options' => $template_options,
      '#states' => [
        'visible' => [
          ':input[name="properties[use_confirmation]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="properties[use_confirmation]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['template_configuration']['success_template_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Mailjet template for success email'),
      '#description' => $this->t('The ID of the mailjet template to use for success email'),
      '#options' => $template_options,
      '#states' => [
        'visible' => [
          ':input[name="properties[use_success]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="properties[use_success]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['list_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Contact list'),
      '#description' => $this->t('The mailjet list ID you would like the user to subscribe to.'),
      '#options' => $list_options,
      '#required' => TRUE,
    ];
    $form['success_node_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Success node ID'),
      '#description' => $this->t('The node to override the default node that is displayed once the user has subscribed successfully.'),
      '#required' => FALSE,
    ];
    $form['always_subscribe_warning'] = [
      '#type' => 'webform_message',
      '#message_type' => 'warning',
      '#message_message' => $this->t('"Always subscribe" is enabled. Depending on the use of the mail list, client interaction in order to subscribe may be a legal requirement in your area.'),
      '#access' => TRUE,
      '#states' => [
        'visible' => [
          [':input[name="properties[always_subscribe]"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) : void {
    if ($form_state->getValue('success_node_id')) {
      $node = $this->entityTypeManager->getStorage('node')->load(intval($form_state->getValue('success_node_id')));
      if (!$node) {
        $form_state->setErrorByName('success_node_id', 'Node does not exist');
      }
    }

    if (!$form_state->getValue('always_subscribe')) {
      if (!$form_state->getValue('user_confirmation__title')) {
        $form_state->setErrorByName('user_confirmation__title', $this->t('Field required if "Always subscribe" is enabled.'));
      }
    }

    if ($form_state->getValue('use_confirmation')) {
      if (!$form_state->getValue('confirm_template_id')) {
        $form_state->setErrorByName('confirm_template_id', $this->t('Field required if "Send confirmation email" is enabled.'));
      }
      if (!$form_state->getValue('template_redirect_variable')) {
        $form_state->setErrorByName('template_redirect_variable', $this->t('Field required if "Send confirmation email" is enabled.'));
      }
    }

    if ($form_state->getValue('use_success') && !$form_state->getValue('success_template_id')) {
      $form_state->setErrorByName('success_template_id', $this->t('Field required if "Send success email" is enabled.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function preSave(array &$element, WebformSubmissionInterface $webform_submission, $update = TRUE) : void {
    parent::preSave($element, $webform_submission, $update);

    if (!$webform_submission->isNew()) {
      return;
    }

    $element_machine_name = $element['#webform_key'];
    $data = $webform_submission->getData();

    $first_name = $data[$element['#first_name_field'] ?? NULL] ?? NULL;
    $last_name = $data[$element['#last_name_field'] ?? NULL] ?? NULL;
    $email_address = $data[$element['#email_address_field']] ?? NULL;
    $accept_mailing_list = $data[$element_machine_name]['user_confirmation'] ?? NULL;

    $confirm_template_id = intval($element['#confirm_template_id'] ?? NULL);
    $success_template_id = intval($element['#success_template_id'] ?? NULL);
    $template_name_variable = $element['#template_name_variable'] ?? NULL;
    $template_redirect_variable = $element['#template_redirect_variable'] ?? NULL;
    $list_id = intval($element['#list_id']);

    $use_confirmation = $element['#use_confirmation'] ?? FALSE;
    $use_success = $element['#use_success'] ?? FALSE;
    $always_subscribe = $element['#always_subscribe'] ?? FALSE;

    if (!$always_subscribe && !$accept_mailing_list) {
      return;
    }

    if (in_array(NULL, [$list_id, $email_address])) {
      \Drupal::logger('mailjet_webform_subscription')->error(
        $this->t(
          'Incorrect or nonexistent field when adding contact. List id: @listId, First Name: @firstName, Last Name: @lastName, Email:@email',
          [
            '@listId' => $list_id,
            '@firstName' => $first_name,
            '@lastName' => $last_name,
            '@email' => $email_address,
          ]
        )
      );
      return;
    }

    if (in_array('', [$list_id, $email_address])) {
      \Drupal::logger('mailjet_webform_subscription')->error(
        $this->t(
          'Empty required form field when adding contact. List id: @listId, First Name: @firstName, Email:@email',
          [
            '@listId' => $list_id,
            '@firstName' => $first_name,
            '@email' => $email_address,
          ]
        )
      );
      return;
    }

    $email_template_variables = [];
    if ($template_name_variable) {
      $email_template_variables[$template_name_variable] = $first_name ?? '';
    }

    if (!$use_confirmation) {
      $success = $this->mailjetApiWrapper->appendToList($email_address, $first_name, $last_name, $list_id);

      if (!$success) {
        \Drupal::logger('mailjet_webform_subscription')->error(
          $this->t(
            'Error occurred appending contact to list. List id: @listId, First Name: @firstName, Last Name: @lastName, Email:@email',
            [
              '@listId' => $list_id,
              '@firstName' => $first_name,
              '@lastName' => $last_name,
              '@email' => $email_address,
            ]
          )
        );
      }

      if ($use_success && $success_template_id) {
        $this->mailjetApiWrapper->sendTemplateEmail(
          $email_address,
          $success_template_id,
          $email_template_variables,
        );
      }

      return;
    }

    $token = hash('sha256', random_bytes(55));
    $data[$element_machine_name]['token'] = $token;
    $webform_submission->setData($data);

    $email_template_variables[$template_redirect_variable] = Url::fromRoute(
        'mailjet_webform_subscription.confirm_mailjet_subscription',
        [],
        [
          'query' => [
            'token' => $token,
            'caller' => $element_machine_name,
          ],
        ]
      )->setAbsolute()->toString();

    $this->mailjetApiWrapper->sendTemplateEmail($email_address, $confirm_template_id, $email_template_variables);
  }

  /**
   * Get valid inputs from the form formatted for a select input.
   */
  private function getAvailableFormFields(): array {
    $result = [];
    foreach ($this->webform->getElementsDecodedAndFlattened() as $key => $value) {
      if ($value['#type'] == 'email' || $value['#type'] == 'textfield') {
        $result[$key] = $value['#title'] . " ($key)";
      }
    }

    return $result;
  }

}
