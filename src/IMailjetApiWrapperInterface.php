<?php

namespace Drupal\mailjet_webform_subscription;

/**
 * Service to connect to mailjet api.
 */
interface IMailjetApiWrapperInterface {

  /**
   * Append to the mailjet mail list .
   *
   * @param string $email_address
   *   The email to add to list.
   * @param string|null $first_name
   *   The first name of subscriber.
   * @param string|null $last_name
   *   The last name of the subscriber.
   * @param int $list_id
   *   The ID of the list to subscribe to.
   */
  public function appendToList(string $email_address, string|null $first_name, string|null $last_name, int $list_id) : bool;

  /**
   * Send templated email.
   *
   * @param string $recipient_email
   *   The email address to send to.
   * @param int $template_id
   *   The id of the MailJet template to use.
   * @param array $variables
   *   Variables to append to send to the template. e.g. [first_name => 'Jeff'].
   */
  public function sendTemplateEmail(string $recipient_email, int $template_id, array $variables = []): void;

  /**
   * Get mail lists from mailjet formatted for a select input.
   *
   * @throws \Exception
   *   If there is an issue with the mailjet api.
   */
  public function getMailJetLists() : array|null;

  /**
   * Get email templates from mailjet formatted for a select input.
   *
   * @throws \Exception
   *   If there is an issue with the mailjet api.
   */
  public function getMailjetTemplates() : array;

}
