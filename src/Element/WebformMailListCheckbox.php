<?php

namespace Drupal\mailjet_webform_subscription\Element;

use Drupal\webform\Element\WebformCompositeBase;

/**
 * Provides a webform element for an address element.
 *
 * @FormElement("mail_list_checkbox")
 */
class WebformMailListCheckbox extends WebformCompositeBase {

  /**
   * {@inheritdoc}
   */
  public static function getCompositeElements(array $element) {
    $elements = [];

    if (!($element['#use_confirmation'] ?? FALSE)) {
      $elements['token'] = [
        '#type' => 'hidden',
        '#title' => t('Token'),
        '#default_value' => '',
      ];

      $elements['token_consumed'] = [
        '#type' => 'hidden',
        '#title' => t('Consumed'),
        '#default_value' => '',
      ];
    }

    if (!($element['#always_subscribe'] ?? FALSE)) {
      $elements['user_confirmation'] = [
        '#type' => 'checkbox',
        '#title' => $element['#user_confirmation__title'] ?? t('Subscribe to newsletter'),
        '#help' => $element['#user_confirmation__help'] ?? '',
        '#required' => $element['#user_confirmation__required'] ?? FALSE,
        '#required_error' => $element['#user_confirmation__required_error'] ?? NULL,
      ];
    }
    return $elements;
  }

}
