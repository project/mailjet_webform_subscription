# Mailjet Webform Subscription

This module offers a user-friendly checkbox element that seamlessly
integrates with MailJet, enabling the addition of clients to a contact list
from any Webform containing an email field.
Additionally, it provides a subscription confirmation and an optional
subscription success email based on MailJet templates.

For more information please visit the module page:
https://drupal.org/project/mailjet_webform_subscription.

## Installation

Install as usual, see
https://www.drupal.org/docs/extending-drupal/installing-modules for further
information.

## Usage

From the client's perspective:
(every one of these steps in configurable and can be turned off)

1. The client will see a mail list checkbox on the WebForm.
2. If selected, upon submission of the form, the client will receive a
   confirmation email containing a one-time confirmation URL.
3. Should this URL be utilized, the client will be added to the mailing list
   and will subsequently receive a confirmation email.

## Configuration

1. Please configure the MailJet API keys at
  _/admin/config/services/mailjetapi/settings_ and enable
  "Use mailjet templates".
2. Ensure that your MailSystem configuration located at
  _/admin/config/system/mailsystem_ is appropriately set so that the
  **Mailjet Webform Subscription** module utilizes the Mailjet API mailer as
  both a **Formatter** and a **Sender**.
3. Incorporate the component type 'Mailjet Subscription' into your Webform.
4. On the component settings page, please fill in the following fields:
   - Form field mapping:
     - **First name field (optional dropdown)** - The field in the form
       designated for obtaining the first name.
     - **Last name field (optional dropdown)** - The field in the form
       designated for obtaining the last name.
     - **Email address field (dropdown)** - The field in the form designated
       for obtaining the email address.
   - Send Client emails:
     - **Send confirmation email** - Enable this to mail the user to confirm
       the subscription.
     - **Send success email** - Enable this to mail the user a success email.
     - **Template name variable (optional textfield)** - The variable
       representing the name utilized by Mailjet templates; for example,
     `Hello {{var:first_name}}` would correspond to `first_name`.
     - **Template redirect variable (textfield)** - The variable representing
       the URL used by the Mailjet confirmation template; for example,
       `Confirm subscription here {{var:redirect_url}}` would correspond to
       `redirect_url`.
     - **Mailjet template for confirmation email**.
     - **Mailjet template for success email**.
   - **Always subscribe** - Remove the checkbox for the user and just continue
     as if accepted. (not recommended unless you know what you are doing)
   - **Success node ID (optional textfield)** - The node to be displayed upon
     completion of subscription, which will override the default success page.
   - **Contact list (dropdown)** - The contact list to which the user will
     subscribe.

## Maintainers

Current maintainers:

* Alex Stanciu (https://www.drupal.org/u/alexst-0).
* Kieran Powell (https://www.drupal.org/u/kieranpw).
* Stefan Matijevic (https://www.drupal.org/u/stefika).
* Stefan Weber (https://www.drupal.org/u/stefanweber).

## Sponsors

Companies who have sponsored this project:

* [1xINTERNET](https://www.1xinternet.de).
* [Coworks](https://www.drupal.org/coworksbe).
